package Basics;

 class inhertanceclass {
	
	void test1() {
		System.out.println("single Inheritance example");
	}

}
 
 class inheritance2 extends inhertanceclass {
	 void test3() {
		 System.out.println("Multilevel Inheritance Example");
	 }
 }

 class TestInheritance extends inheritance2 {
	void test2() {
		System.out.println("TestInheritance");
	}
	
	public static void main(String[] args) {
		TestInheritance objInher = new TestInheritance();
		objInher.test1();
		objInher.test3();
		objInher.test2();
		
	}
	
}


//class Animal {
//	void eat() {
//		System.out.println("eating...");
//	}
//}
//
//class Dog extends Animal {
//	void bark() {
//		System.out.println("barking...");
//	}
//}
//
//class TestInheritance {
//	public static void main(String args[]) {
//		Dog d = new Dog();
//		d.bark();
//		d.eat();
//	}
//}
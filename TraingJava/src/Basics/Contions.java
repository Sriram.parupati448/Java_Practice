package Basics;

public class Contions {
	int i=30;
	int j=20;
	//char grade = 'A';
	int y = 15;
	
	public static void main(String[] args) {
		
		
		
		Contions objMaking = new Contions();
		objMaking.ifcondion();
		objMaking.ifelseCondition();
		objMaking.ifelseifcondions();
		objMaking.switchcase();
		objMaking.forIncreamentloop();
		objMaking.forDecrementloop();
		objMaking.whileloop();
		objMaking.doloop();
		

	}
	
	public void ifcondion() {
		if (i<40) {
			System.out.println("this is if condition");
		}
	}
	
	public void ifelseCondition() {
		if (i<10) {
			System.out.println("this is if condition" );
		} else {
			System.out.println("this is else condition");
		}
			
	}
	
	public void ifelseifcondions(){
		
		if (i==10) {
			System.out.println("value i equal to 10");
		} else if (i==20) {
			System.out.println("value i equal to 20");
		} else if (i==30) {
			System.out.println("value i equal to 30");
		} else 
		System.out.println("this is else case");
	}

	public void switchcase() {
		
		char grade = 'D';
		
		switch(grade) {
		
		case 'A' :
			System.out.println("Excellent");
			break;
		case 'B' :
			System.out.println("Well done");
			break;
		case 'C' :
			System.out.println("Good Job");
		break;
	case 'D' :
	case 'E' :
			System.out.println("Better to try again");
		break;
		default :
			System.out.println("Invalid grade");
		
		}
		System.out.println("Your grade is " + grade);
	}
	
	public void forDecrementloop() {
		for(int x=10; x>=1;x--) {
			System.out.println("value of x: " + x);
		}
	}
	
	public void forIncreamentloop() {
		for(int x=10; x<20;x++) {
			System.out.println("value of x: " + x);
		}
	}
	
	public void whileloop() {
		
		while (i <= 40) {
			System.out.println("value of i: " + i);
			i++;
		}
	}
	
	public void doloop() {
		int y = 10;
		do {
	         System.out.print("value of y : " + y );
	         y++;
	         System.out.print("\n");
	      }while( y < 20 );
	}
	}


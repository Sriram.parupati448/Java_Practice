package Basics;

public class MethodOverriding extends A{
	
	void msg() {
		System.out.println("This is venkat");
	}
	
	int Add(int X, int Y) {
		return X+Y;
//		return c;
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MethodOverriding OBJ = new MethodOverriding();
		OBJ.msg();
		System.out.println("Add two values: "+OBJ.Add(10, 20));
	}

}

class A{
	
	int Add(int i, int j) {
		return i+j;
	}
}

class B extends A{
	
	int Add(int K, int L) {
		return K+L;
	}
}

class C extends A{
	
	int Add(int M, int N) {
		return M+N;
	}
}